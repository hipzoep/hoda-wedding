import React, { Component } from 'react';
 
import Task from './Task.js';
import Main from './Main.js';
import Phone from './Phone.js';
import Gallery from './Gallery.js';
import Callender from './Callender.js';
import LocationInfo from './LocationInfo.js';
import Footer from './Footer.js';
 
// App component - represents the whole app
export default class App extends Component {
  getTasks() {
    return [
      { _id: 1, text: 'This is task 1' },
      { _id: 2, text: 'This is task 2' },
      { _id: 3, text: 'This is task 3' },
    ];
  }
 
  renderTasks() {
    return this.getTasks().map((task) => (
      <Task key={task._id} task={task} />
    ));
  }
 
  render() {
    return (
      <div className="container">
        <Main />
        <Phone />
        <Gallery />
        <Callender />
        <LocationInfo />
        <Footer />
      </div>
    );
  }
}
