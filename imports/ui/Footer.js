import React, { Component } from 'react';

// Task component - represents a single todo item
export default class Footer extends Component {
  componentDidMount() {
    Kakao.Link.createCustomButton({
      container: '#kakao-template-id',
      templateId: 11530,
      templateArgs: {}
    });
  }
  render() {
    return (
        <div id="kakao-template-id" className="footer">
            <img id="linkImg" src="images/kakao.png" />
            <img id="linkText" src="images/link-text.png" />
        </div>
    );
  }
}
